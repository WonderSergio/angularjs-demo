'use strict';


  angular.module('angularjsDemoApp', [
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'ui.router'
])
  .config(function ($stateProvider, $urlRouterProvider, $locationProvider) {
    $urlRouterProvider.otherwise('/');
    $stateProvider
      .state('index', {
        url: '/',
        templateUrl: 'views/main.html',
        controller:'MainCtrl'
      })
      .state('tasks', {
        url: '/tasks',
        templateUrl: 'views/tasks.html',
        controller: 'TasksCtrl'
      });

    $locationProvider.html5Mode(true);

  })




