'use strict';

angular.module('angularjsDemoApp')
  .controller('MainCtrl', function ($scope) {
    $scope.menuItems = [
      'tasks',
      'done',
      'summary'
    ];
  });
