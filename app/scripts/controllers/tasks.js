'use strict';

angular.module('angularjsDemoApp')
  .controller('TasksCtrl', function ($scope) {
    $scope.tasks = [];

    $scope.addTask = function(name) {
      $scope.tasks.push({name: name, checked: false});
    }
  });
